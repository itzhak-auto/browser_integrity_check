BIC - Browser Integrity Check

A php script to check if a visitor is a bot or a human by checking if
the end device supports

1. Cookies
2. HTTP Push
3. JavaScript

If the end device does, it will be installed with an authenticty cookie
to prevent this check from accuring for a limited time.

				!!!WARNING!!!

The script requires 'php5-mcrypt', without it your WordPress will break
and you will need to manually delete this folder. Also, don't forget to
change the default 'KEY' and 'Contact' details.