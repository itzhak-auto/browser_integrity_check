<?php
/*
Plugin Name:    iTK98 Browser Intergrity Check (BIC)
Description:    Small script to check Cookies, Post, JavaScript functionality in the browser in order to complete a challenge.
Author:		Itzhak Daniel.
Version:        0.02
Author URI:     https://www.itk98.net
*/

  defined( 'ABSPATH' ) or die( 'No direct access allowed' );

  // Adding the script to the init of WordPress
  add_action( 'init', 'itk98_bic', 0 );

  function itk98_bic() {
    define( 'ITK98_BIC_PATH', plugin_dir_path( __FILE__ ) );
    define( 'ITK98_BIC_URL', plugin_dir_url( __FILE__ ) );

    require_once ITK98_BIC_PATH . 'include/main.php';
  }

?>
