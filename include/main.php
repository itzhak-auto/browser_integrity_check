<?php
  // For WordPress, no direct access
  defined( 'ABSPATH' ) or die( 'No direct access allowed!' );

  /*************************************************************
  * A Script to check browser capabilites, JavaScript,         *
  * Cookies and HTTP-Post. When the challenge is successfully  *
  * completed a 24h cookie will be stored until next check-up. *
  *                                                            *
  * By Itzhak Daniel (iTK98).                                  *
  *  License, Public Domain.                                   *
  *************************************************************/

// No cache
  header("Cache-Control: no-store, no-cache, must-revalidate"); 
  header("Expires: off");

// Key, change it to your own
  $itk98_bic_key = "CHANGE ME!";					// Key to encrypt the cookie
  $itk98_bic_contact = "CHANGE ME!";					// Contact email
  $itk98_bic_expire = time()+86400;					// Cookie life, 24h
  $itk98_bic_hash = "sha256";						// Hash Algorithm:
									// Common: md5, sha1, sha256, sha512 | https://secure.php.net/manual/en/function.hash-algos.php
  $itk98_bic_cipher = "rijndael-256";					// Ciphername:
									// Common: 3des, blowfish, twofish, crypt, rijndael-128, rijndael-256 | http://www.php.net/manual/en/mcrypt.ciphers.php
  $itk98_bic_cpm ="ofb";						// Block mode:
									// ecb, cbc, cfb, ofb, nofb, stream | https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation

  $itk98_bic_ua = $_SERVER['HTTP_USER_AGENT'];
  $itk98_bic_addr = $_SERVER['REMOTE_ADDR'];


// Encrypting the cookie material to avoid forgery.
// Using TIME+$itk98_bic_expire, User agent of the browser and
// the client IP address.
  function itk98_bic_enc($itk98_bic_key, $itk98_bic_expire, $itk98_bic_contact, $itk98_bic_ua, $itk98_bic_addr, $itk98_bic_hash, $itk98_bic_cipher, $itk98_bic_cpm) {
    $itk98_bic_data = "$itk98_bic_expire" . "|" . "$itk98_bic_ua" . "$itk98_bic_addr";

    $itk98_bic_td = mcrypt_module_open("$itk98_bic_cipher", '', "$itk98_bic_cpm", '');
    $itk98_bic_iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($itk98_bic_td), MCRYPT_DEV_URANDOM);
    $itk98_bic_ks = mcrypt_enc_get_key_size($itk98_bic_td);
    $itk98_bic_key2 = substr(hash("$itk98_bic_hash", "$itk98_bic_key"), 0, $itk98_bic_ks);
    mcrypt_generic_init($itk98_bic_td, $itk98_bic_key2, $itk98_bic_iv);

    $itk98_bic_edata = base64_encode(mcrypt_generic($itk98_bic_td, $itk98_bic_data));
    $itk98_bic_IV = base64_encode("$itk98_bic_iv");
    mcrypt_generic_deinit($itk98_bic_td);
    return $itk98_bic_IV . "|" . $itk98_bic_edata;				// returning the encrypted cookie
  }

// Decrypting the cookie, making sure the values match.
// if the cookie was forged the client will be blocked
// until the cookies removed from the system.
  function itk98_bic_dec($itk98_bic_key, $itk98_bic_expire, $itk98_bic_contact, $itk98_bic_ua, $itk98_bic_addr, $itk98_bic_hash, $itk98_bic_cipher, $itk98_bic_cpm) {
    if ((strlen($_COOKIE['BIC_ID']) > 256) || (strlen($_COOKIE['BIC_HASH']) > 256)) {
      unset($_COOKIE['BIC_PRE']);
      unset($_COOKIE['BIC_ID']);
      unset($_COOKIE['BIC_HASH']);
      setcookie('BIC_PRE', '', 1, "/");
      setcookie('BIC_ID', '', 1, "/");
      setcookie('BIC_HASH', '', 1, "/");
      die("Cookie Monster!");
    }
    $itk98_bic_edata = $_COOKIE['BIC_ID'];
    $itk98_bic_parts = explode("|",$itk98_bic_edata);

    $itk98_bic_td = mcrypt_module_open("$itk98_bic_cipher", '', "$itk98_bic_cpm", '');
    $itk98_bic_iv = base64_decode("$itk98_bic_parts[0]");
    $itk98_bic_ks = mcrypt_enc_get_key_size($itk98_bic_td);
    $itk98_bic_key2 = substr(hash("$itk98_bic_hash", "$itk98_bic_key"), 0, $itk98_bic_ks);

    mcrypt_generic_init($itk98_bic_td, $itk98_bic_key2, $itk98_bic_iv);
    $itk98_bic_data = explode("|",mdecrypt_generic($itk98_bic_td, base64_decode("$itk98_bic_parts[1]")));
    mcrypt_generic_deinit($itk98_bic_td);
    mcrypt_module_close($itk98_bic_td);

    if (("$itk98_bic_data[0]" - time() < 0) || ("$itk98_bic_data[1]" != "$itk98_bic_ua" . "$itk98_bic_addr"))
      return 1;
    else
      return 0;
  }

// A list of IP address that are white list
// I recommand to whitelist the major search
// engines ranges, I added few.
  function whitelist() {
    $itk98_bic_whitelist = array(
                       "^127\.",				// Localhost, for internal scripts
                       "^64\.18\.\[0-15\]\.",			// Google, IPv4
                       "^64\.233\.1\[6-9\]|\[9\]\[1\]\.",
                       "^66\.102\.\[0-15\]\.",
                       "^66\.249\.\[64-95\]\.",
                       "^72\.14\.\[192-255\]\.",
                       "^74\.125\.",
                       "^108\.177\.\[8-15\]\.",
                       "^173\.194\.",
                       "^207\.216\.\[144-159\]\.",
                       "^209\.85\.\[128-255\]\.",
                       "^216\.58\.\[192-223\]\.",
                       "^216\.239\.\[32-63\]\.",
                       "^2001\:4860\:4000\:",			// Google, IPv6
                       "^2404\:6800\:4000\:",
                       "^2607\:f8b0\:4000\:",
                       "^2800\:03f0\:4000\:",
                       "^2a00\:1450\:4000\:".
                       "^2c0f\:fb50\:4000\:",
                       "^64\.4\.\[0-63\]\.",			// Bing
                       "^65\.52.\.",
                       "^131\.253\.\[21-47\]\.",
                       "^157\.\[54-60\]\.",
                       "^207\.46\.",
                       "^207\.68\.\[128-207\]\.",
                       "^31\.13\.\[24-31\]\.",			// Facebook, IPv4
		               "^31\.13\.\[64-95\]\.",
        		       "^66\.220\.\[144-159\]\.",
		               "^69\.63\.\[176-191\]\.",
	        	       "^69\.171\.\[224-255\]\.",
		               "^74\.119\.\[76-79\]\.",
		               "^173\.252\.\[64-127\]\.",
		               "^204\.15\.\[20-23\]\.",
		               "^2a03\:2880\:",			    	// Facebook, IPv6
                       "^72\.94\.249\.3\[4-8\]",		// DuckDuckGo
    );

    foreach($itk98_bic_whitelist as $itk98_bic_range) {
      if (ereg($itk98_bic_range, $_SERVER['REMOTE_ADDR']))
        $itk98_bic_whitelist_true = 1;
        break 1;
    }
    if ($itk98_bic_whitelist_true)
      return 1;
    else
      return 0;
  }

// The body of the code
  function challenge($itk98_bic_key, $itk98_bic_expire, $itk98_bic_contact, $itk98_bic_ua, $itk98_bic_addr, $itk98_bic_hash, $itk98_bic_cipher, $itk98_bic_cpm) {
    if (!isset($_COOKIE['BIC_ID'])) {
      if (isset($_POST['_authorized'])) {
        if (!isset($_COOKIE['BIC_PRE']) || ($_COOKIE['BIC_PRE'] != '1')) {
          echo '
          <html><head><title>Browser Integrity Check - Failed</title>
          <style type="text/css">html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
          body {background-color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 100%;}
          </style></head><body>
          <table width="100%" height="100%" cellpadding="20"><tr><td align="center" valign="middle">
          <img src="';
          echo ITK98_BIC_URL . 'image/icon.ico' . '" />';
          echo '
          <h1>Browser Integrity Check</h1>
          <p>Your browser doesn\'t seem to support cookies.</p>
          <p>Please enable cookies in your browser and try again.</p>
          </body>
          </html>
          ';
          die;
        }
        else {
          $itk98_bic_tmp_p = $_POST['_authorized'];
          setcookie('BIC_ID', $itk98_bic_tmp_p, $itk98_bic_expire, "/");
          setcookie('BIC_HASH', hash("$itk98_bic_hash", "$itk98_bic_tmp_p" . "$itk98_bic_key"), $itk98_bic_expire, "/");
        }
      }
      else {
        setcookie('BIC_PRE', '1', time() + 30, "/");
        echo '

        <html><head><title>Browser Integrity Check</title>
        <style type="text/css">html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
        body {background-color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 100%;}
        </style></head><body>
        <table width="100%" height="100%" cellpadding="20"><tr><td align="center" valign="middle">
        <img src="';
        echo ITK98_BIC_URL . 'image/icon.ico' . '" />';
        echo '
        <h1>Browser Integrity Check</h1>
        <p>Please wait 3 seconds, the page will auto refresh.</p>
        <noscript><p>JavaScript is disabled, cannot continue. Please make sure your browser is supporting Cookies, JavaScript and POST.</p></noscript></td></tr></table>
        <form method="post">
        <button type="submit" name="_authorized" style="display: none" id="_authorized" value="' . itk98_bic_enc($itk98_bic_key, $itk98_bic_expire, $itk98_bic_contact, $itk98_bic_ua, $itk98_bic_addr, $itk98_bic_hash, $itk98_bic_cipher, $itk98_bic_cpm) . '"/>
        </form>
        <script>
        setTimeout(function(){document.getElementById("_authorized").click()},3000)
        </script>
        </body>
        </html>
        ';
        die;
      }
    }
    else {
      if (itk98_bic_dec($itk98_bic_key, $itk98_bic_expire, $itk98_bic_contact, $itk98_bic_ua, $itk98_bic_addr, $itk98_bic_hash, $itk98_bic_cipher, $itk98_bic_cpm) || ($_COOKIE['BIC_HASH'] != hash("$itk98_bic_hash", "$_COOKIE[BIC_ID]" . "$itk98_bic_key"))) {
        echo '
        <html><head><title>Browser Integrity Check - Failed</title>
        <style type="text/css">html, body {width: 100%; height: 100%; margin: 0; padding: 0;}
        body {background-color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 100%;}
        </style></head><body>
        <table width="100%" height="100%" cellpadding="20"><tr><td align="center" valign="middle">
        <img src="';
        echo ITK98_BIC_URL . 'image/icon.ico' . '" />';
        echo '
        <h1>Browser Integrity Check</h1>
        <p>Wrong value found in cookies. Please manually remove all domain related cookies and reload this page.</p>
        <p>If this error is persistent, Please contact <a href="mailto:';
        echo "$itk98_bic_contact" . '">' . "$itk98_bic_contact" . '</a></p>';
        echo '
        </body>
        </html>
        ';
        unset($_COOKIE['BIC_PRE']);
        unset($_COOKIE['BIC_ID']);
        unset($_COOKIE['BIC_HASH']);
        setcookie('BIC_PRE', '', 1, "/");
        setcookie('BIC_ID', '', 1, "/");
        setcookie('BIC_HASH', '', 1, "/");
        die;
      }
    }
  }

// Checking if the IP address is whitelisted
  if (!whitelist()) {			// Otherwise running the challenge.
    challenge($itk98_bic_key, $itk98_bic_expire, $itk98_bic_contact, $itk98_bic_ua, $itk98_bic_addr, $itk98_bic_hash, $itk98_bic_cipher, $itk98_bic_cpm);
    unset($itk98_bic_white);
  }
?>
